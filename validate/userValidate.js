const Users = require('../configs/db')
var passwordValidator = require('password-validator');

module.exports = {

    userDuplicateValidate: (req, res, next) => {
        const username = req.body.username
        Users.findOne({
                where: {
                    username: username
                },
            }).then(
                (raw) => {
                    if(raw)
                    {
                        res.send('username existed !!!!!!!!!!')
                    } else
                    next()
                }
            )
            .catch((err) => {
                res.json(err)
            })
    },
    userNonExist:(req, res, next) => {
        const {id} = req.body
        Users.findOne({
                where: {
                    id: id
                },
            }).then(
                (raw) => {
                    if(raw)
                    {
                        next()
                    } else
                    res.send('User have not existed !!!!!!!!!!')
                }
            )
            .catch((err) => {
                res.json(err)
            })
    },
    userPasswordValidate:(req, res, next) => {
        var schema = new passwordValidator();
        const {password} = req.body
        schema
            .is().min(8)                                    // Minimum length 8
            .is().max(100)                                  // Maximum length 20
            .has().uppercase()                              // Must have uppercase letters
            .has().lowercase()                              // Must have lowercase letters
            .has().digits()                                 // Must have digits
            .has().not().spaces()                           // Should not have spaces
            .is().not().oneOf(['Passw0rd', 'Password123']); // Blacklist these values
        if(schema.validate(password)){
            next()
        }else{
            res.send(' password is not illegal')
        }
    
        },
}