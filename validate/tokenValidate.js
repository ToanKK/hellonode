var jwt = require('jsonwebtoken')
var tokenConfig = require('../configs/tokenConfig')
module.exports = {
   tokenValidate: (req,res,next)=>{
    if(req.headers && req.headers.authorization ){
         var token = req.headers.authorization;
        jwt.verify(token,tokenConfig.secret,(err,decode)=>{
            if(err){
                return res.status(401).send({
                    message:'Token invalid'
                })
            }else
                return next()
        })
    }else{
        return res.status(401).send({
            message:'Unauthorized'
        })
    }
}
  }