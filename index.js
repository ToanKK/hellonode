const app = require('express')();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const userRoute = require('./routes/user.route')
const authRoute = require('./routes/user.auth')
const upRoute = require('./routes/user.upload')
const chatRoute = require('./routes/user.chat')
const bodyParser = require('body-parser')
var jwt = require('jsonwebtoken')
const tokenConfig = require('./configs/tokenConfig')
app.use(bodyParser.urlencoded({
    extended: false,
}))
app.use(bodyParser.json());
const mongodbConfig = require('./configs/mongodbConfig')


let users = []
let sockets = []

mongodbConfig.client.connect((err) => {
    const ChatAll = mongodbConfig.client.db("ChatDB").collection("ChatAll");
    const ChatPersonal = mongodbConfig.client.db("ChatDB").collection("ChatPersonal");
    const ListRoomChats = mongodbConfig.client.db("ChatDB").collection("ListRoomChats");
    console.log('success!!!')
    io.on('connection', function (socket) {
        let oldMessages = []

        function errorFunc(error) {
            console.log(error);
        }
        console.log('client - signIn')

        function iterateFunc(doc) {
            let msg = JSON.parse(JSON.stringify(doc, null, 4))
            console.log(msg)
            oldMessages.push(msg)
        }
        const x = ChatAll.find({})
        x.forEach(iterateFunc, errorFunc)

        socket.on('client-SignIn', async () => {
            socket.emit('server-send-old-msg', oldMessages)
        })
        let listRum = [];
        socket.auth = false;
        socket.on('authenticate', function (data) {
            jwt.verify(data.token, tokenConfig.secret, (err, decode) => {
                if (err) {
                    console.log(err.message)
                } else {
                    socket.auth = true;
                    console.log("Authenticated socket -----", socket.id, socket.auth);
                    console.log('decode', decode)
                    socket.userId = decode.id
                    ListRoomChats.findOne({
                            "user_id": socket.userId
                        },
                        async (err, res) => {
                            if (err) throw err;
                            await res.listRooms.forEach(
                                (element) => {
                                    listRum.push(element)
                                    console.log(element)
                                    socket.join(element.roomName)
                                }
                            )
                            socket.emit('getListRoom', Object.keys(socket.rooms))

                        }
                    )

                }
            })
        });


        console.log('new user connected', socket.id)
        socket.on("client-send-Username", (data) => {
            if (users.includes(data)) {
                socket.emit('server-send-dki-that-bai');
            } else {
                users.push(data)
                socket.userName = data
                sockets.push(socket)
                socket.emit('server-send-dki-thanhcong', data);
                io.emit('send-list-users', users)
            }
        })

        //listen user send mess
        socket.on('user-send-message', async (data) => {
            if (socket.auth === true) {
                io.emit('server-send-message', {
                    un: socket.userName,
                    nd: data
                })
                try {
                    await ChatAll.insertOne({
                        'name': socket.userName,
                        'message': data
                    })
                } catch (err) {
                    console.log(err)
                }

            } else {
                return 0;
            }
        })
        // io.to(`${socket.id}`).emit('getListRoom', Object.keys(socket.rooms))
        // listen event create room 
        socket.on('send-creation-to-server', async (roomName, user) => {
            socket.join(roomName)
            socket.emit('getListRoom', [roomName])

            //socket is invited to room
            const socketInvited = sockets.find(x => x.userName === user)
            console.log('socketID can tim ', socketInvited.userId)
            const idRoomChat = Date.now();
            listRum.push({
                roomId: idRoomChat,
                roomName: roomName
            })
            ChatPersonal.insertOne({
                _id: idRoomChat,
                nameRoom: roomName,
                participants: [socket.userId, socketInvited.userId],
                messages: [],
            })
            ListRoomChats.updateOne({
                "user_id": socket.userId
            }, {
                $push: {
                    listRooms: {
                        roomId: idRoomChat,
                        roomName: roomName
                    }
                }
            })
            const roomInfo = {
                roomName: roomName,
                idRoomChat: idRoomChat
            }

            io.to(socketInvited.id).emit('server-send-add-user', roomInfo)

        })
        // listen event add user to room
        socket.on('add-user-to-room', async (data) => {
            console.log('my data', data)
            console.log(socket.userId)
            listRum.push({
                roomId: data.idRoomChat,
                roomName: data.roomName
            })
            socket.join(data.roomName)
            socket.emit('getListRoom', [data.roomName])
            await ListRoomChats.update({
                "user_id": socket.userId
            }, {
                $push: {
                    listRooms: {
                        roomId: data.idRoomChat,
                        roomName: data.roomName
                    }
                }
            })
        })

        socket.on('disconnect', () => {
            const index = users.indexOf(socket.userName)
            users.splice(index, 1)
            io.emit('send-list-users', users)
        })

        socket.on('user-send-message-room', (roomId, msg) => {
            console.log('list all rooms', socket.rooms)
            io.to(`${roomId}`).emit('server-send-msg-toroom', {
                un: socket.userName,
                nd: msg
            });
            const room = listRum.find((e) => e.roomName === roomId)
            console.log('find room', listRum)
            console.log('find room', roomId)
            console.log('find room', room)


            ChatPersonal.update({
                _id: room.roomId
            }, {
                $push: {
                    messages: {
                        sender: socket.userName,
                        content: msg,

                    }
                }
            })

        })




    })



});




app.use('/users', userRoute)
app.use('/auth', authRoute)
app.use('/', upRoute)
app.use('/', chatRoute)




server.listen(3000, console.log('server đang chạy port 300'))