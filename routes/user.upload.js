const express = require('express')
const router = express.Router()
const controllers = require('../controllers/user.uploadController')
const validate = require('../validate/signInGoogle')


router.post('/upload' ,controllers.upload)
router.post('/upload2' ,controllers.upload2)


module.exports = router