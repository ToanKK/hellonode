const express = require('express')
const router = express.Router()
const controller = require("../controllers/user.chatController")

router.get('/renderChatBox',controller.renderChatBox)


module.exports = router