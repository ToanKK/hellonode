const express = require('express')
const router = express.Router()
const controller = require('../controllers/user.authController')
const validateToken = require('../validate/tokenValidate')
 router.post('/login',controller.login)
 
 router.post('/otherActtion',validateToken.tokenValidate,controller.actionTestGetToken)

 router.post('/refresh-token',controller.refreshToken)


 module.exports = router