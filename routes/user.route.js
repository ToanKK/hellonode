const express = require('express')
const router = express.Router()
const controllers = require('../controllers/user.controller')
const validate = require('../validate/userValidate')

router.post('/add',validate.userDuplicateValidate,validate.userPasswordValidate,controllers.add)
router.get('/read',controllers.read)
router.post('/update',validate.userNonExist,validate.userDuplicateValidate,validate.userPasswordValidate,controllers.update)
router.post('/delete',validate.userNonExist,controllers.delete)
router.post('/find',controllers.find,)


module.exports = router