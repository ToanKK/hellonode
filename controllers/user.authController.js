const jwt = require('jsonwebtoken');
const config = require('../configs/tokenConfig');
const db = require('../configs/db')
const Users = require('../configs/db')

module.exports = {
    login:(req,res)=>{
        const {username,password} = req.body
        // console.log(req.body)
        // console.log('body',JSON.parse(Object.keys(req.body)))
        const form = JSON.parse(Object.keys(req.body))
        Users.findOne({
            where: {username: form.data.username,password:form.data.password},
        }).then(
        (row)=>{
                    const data = JSON.stringify(row)
                    const vdata = JSON.parse(data)
                    const token = jwt.sign(vdata,config.secret,{algorithm:'HS256',expiresIn:config.tokenLife})
                    const refreshToken = jwt.sign(vdata,config.refreshsecret,{algorithm:'HS256',expiresIn:config.refreshTokenLife})
                         res.json({token,refreshToken})
                   
                }
      )
      .catch(err=>res.json({'kq':err.message}))
        
    },
    refreshToken:(req,res)=>{    
        const tokenOfClient = req.body.resfreshToken
        if(tokenOfClient){
            jwt.verify(tokenOfClient,config.refreshsecret,(err,decode)=>{
                if(err){
                    return res.status(403).send({
                        message:'Token invalid'
                    })
                }else{
                var newToken = jwt.sign({id:decode.id,name:decode.name,map:decode.mail},config.secret,{algorithm:'HS256',expiresIn:config.tokenLife})
                res.json({
                    'newtoken':newToken,
                    'decode':decode,
                })    
                }
                    
            })
        }else{
            res.send('chua co refresh token')
        }
    },
    actionTestGetToken:(req,res)=>{
        res.send('đã có token trong tay !!1')
    }
}