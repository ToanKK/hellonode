const Users = require('../configs/db')
const db = require('../configs/db')
const { QueryTypes } = require('sequelize');
const sequelize = require('sequelize');

module.exports = {
    add:(req,res)=>{
        const{ username,password} = req.body
    
        Users.create({
            username:username,
            password:password
        })
        .then(()=>res.json({'kq':'add ok'}))
        .catch(err=>res.json({'kq':0}))
    },
    read:(req,res)=>{
        Users.findAll()
        .then((users)=>res.json({'kq':"ok","users":users}))
        .catch(err=>res.json({'kq':err.message}))
    },
    update:(req,res)=>{
        const {id,username,password} = req.body
        Users.update({
            username:username,
            password:password
        },{
            where:{id: id}
        })
        .then((row)=>res.json({'kq':1,"row":row[0]}))
        .catch(err=>res.json({'kq':0}))
    
    },
    delete:(req,res)=>{
        const {id} = req.body
        Users.destroy({
            where:{id: id}
        })
        .then((row)=>res.json({'kq':1,"row":row}))
        .catch(err=>res.json({'kq':0}))
    },
    find: (request, res) => {
        const username = request.body.u
        const password = request.body.p

    //     Users.findOne({
    //         where: {username: username,password:password},
    //   }).then((row)=>res.json({'kq':1,"row":row}))
    //   .catch(err=>res.json({'kq':0}))

    sequelize.query("SELECT * FROM `users`", { type: QueryTypes.SELECT })
    .then((row)=>res.json({'kq':1,"row":row}))
    .catch(err=>res.json({'kq':0}))
    }

}